﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    public partial class MyCalc
    {
        public double Plus(double a, double b)
        { return a + b; }

        public double Minus(double a, double b)
        { return a - b; }

        public double Multiply(double a, double b)
        { return a * b; }

        public double Divide(double a, double b)
        {
            if (b != 0)
                return a / b;
            else throw new DivideByZeroException();
        }

        public double Power(double a, double b)
        {
            return Math.Pow(a,b);
        }

        public double Absol(double a)
        {
            return Math.Abs(a);
        }
        /// <summary>
        /// paramert a must be entered a in degrees
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public double Sin(double a)
        {
            double b =a*(1-Math.Floor(a/360));
            return Math.Sin(b*Math.PI/180);
        }

        /// <summary>
        /// paramert a must be entered a in degrees
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public double Cos(double a)
        {
            double b = a * (1 - Math.Floor(a / 360));
            return Math.Cos(b * Math.PI / 180);
        }

        /// <summary>
        /// paramert a must be entered a in degrees
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public double Tan(double a)
        {
            double b = a * (1 - Math.Floor(a / 360));
            return Math.Tan(b * Math.PI / 180);
        }
    }
}
