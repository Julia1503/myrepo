﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calc;
using ConsoleCalc.Resource;


namespace ConsoleCalc
{
  

    public static class WorkWithConsole
    {
        private static MyCalc _myCalc;
        private static Func<double, double, double> delBinar;
        private static Func<double, double> delUnar;
                      
        static WorkWithConsole()
        {
            _myCalc = new MyCalc();
            delBinar = null;
            delUnar = null;
        }

        
        private static double GetOperand()
        {
            double operand = 0;
            bool wasCorrectParsed = false;    
            while (wasCorrectParsed != true)
                {
                    Console.Write(Messages.EnterOperand);
                    string withoutSpace = ReadFromConsole.Read().Trim();
                    wasCorrectParsed = Double.TryParse(withoutSpace, out operand);
                    if (wasCorrectParsed == false) { Console.WriteLine(Messages.WrongOperand); }
                 }
            return operand;
        }
            
        public static void GetOperator()
        { 
            delBinar = null;
            delUnar = null;
            while (delBinar == null && delUnar == null)
            {
                Console.Write(Messages.EnterOperator);
                string operand = ReadFromConsole.Read().Trim();

                switch (operand)
                {
                    case "+": { delBinar = new Func<double, double, double>(_myCalc.Plus); break; }
                    case "-": { delBinar = new Func<double, double, double>(_myCalc.Minus); break; }
                    case "*": { delBinar = new Func<double, double, double>(_myCalc.Multiply); break; }
                    case "/": { delBinar = new Func<double, double, double>(_myCalc.Divide); break; }
                    case "^": { delBinar = new Func<double, double, double>(_myCalc.Power); break; }
                    case "abs": { delUnar = new Func<double, double>(_myCalc.Absol); break; }
                    case "sin": { delUnar = new Func<double, double>(_myCalc.Sin); break; }
                    case "cos": { delUnar = new Func<double, double>(_myCalc.Cos); break; }
                    case "tan": { delUnar = new Func<double, double>(_myCalc.Tan); break; }

                    default: { Console.WriteLine(Messages.WrongOperator); Help(); break; }
                }
            }
        }

        public static double DoWork()
        {
            double firstOperand = GetOperand();
            GetOperator();
            double result;
            if (delBinar != null) { result = delBinar(firstOperand,GetOperand()); }
            else { result = delUnar(firstOperand); }
            return result;
        }

        public static void Help()
        {
            Console.WriteLine(Messages.Help);
        }

 
    }
}
