﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalc
{
    public delegate void MyDelESC();



    public static class ReadFromConsole
    {
        public static event MyDelESC ESCPressed;

        public static string Read()
        {
            ConsoleKeyInfo cki;
            Console.TreatControlCAsInput = true;
            StringBuilder sb = new StringBuilder();
            do
            {

                cki = Console.ReadKey();
                if ((cki.Key != ConsoleKey.Enter) && (cki.Key != ConsoleKey.Escape)) { sb.Append(cki.KeyChar); }

                if (cki.Key == ConsoleKey.Enter) Console.WriteLine();
                if (cki.Key == ConsoleKey.Escape) OnESCPressed();
            } while (cki.Key != ConsoleKey.Enter);
            return sb.ToString();
        }


        public static void OnESCPressed()
        {
            MyDelESC handler = ESCPressed;
            if (handler != null)
            {
                handler();
            }
        }
    }
}
 