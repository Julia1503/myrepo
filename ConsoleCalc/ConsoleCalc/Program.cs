﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleCalc.Resource;

namespace ConsoleCalc
{
    class Program
    {
        static void Main()
        {
            WorkWithConsole.Help();
            while(true)
            {
               ReadFromConsole.ESCPressed += ApplicationClose;
               Work();                    

            }
        }


        public static void Work()
        {
            try
            {
                Console.WriteLine("Ответ: {0}", WorkWithConsole.DoWork());
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine(Errors.DivideByZero);
            }
            catch (FormatException)
            {
                Console.WriteLine(Errors.WrongFormat); 
            }
            catch (OverflowException)
            {
                Console.WriteLine(Errors.Overflow);
            }
        }

        public static void ApplicationClose()
        {
            Environment.Exit(0);
        }
    }
}
